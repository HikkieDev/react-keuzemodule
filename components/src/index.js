import React from 'react';
import reactDom from 'react-dom';
import fakerStatic from 'faker';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

const App = () => {
    return (
        <div className="ui container comments">
            <ApprovalCard>
                <div>
                    <h4>WARNING!</h4>
                Are you sure you want to do this?
                </div>
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail
                    author="Sam"
                    timeAgo="today at 4:45 PM"
                    content="First!"
                    avatar={fakerStatic.image.avatar()}
                />
            </ApprovalCard>

            <ApprovalCard>
                <CommentDetail
                    author="Alex"
                    timeAgo="today at 1:00 PM"
                    content="hahahaha"
                    avatar={fakerStatic.image.avatar()}
                />
            </ApprovalCard>

            <ApprovalCard>
                <CommentDetail
                    author="Jane"
                    timeAgo="today at 5:00AM"
                    content="nice blog man"
                    avatar={fakerStatic.image.avatar()}
                />
            </ApprovalCard>
        </div>
    );
};

reactDom.render(<App />, document.querySelector('#root'))